# ChilliSwap Staking Contracts

## How to Test Locally

Make sure you've [Hardhat](https://hardhat.org/) installed (Hardhat is an alternative to Truffle Suite).

**Clone the repo**
```
git clone 
```
**Install Dependencies**
```
npm install
```

**Start Local Hardhat Node**
```
npx hardhat node
```

**Run Tests**
```
npx hardhat test
```


npx hardhat run --network kovan scripts/deploy.js

